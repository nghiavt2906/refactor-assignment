public class BaseView
{
	public void CloseView()
	{
		DoReset();
	}
	
	protected virtual void DoAction()
	{
		if (!(this is UnrulyView))
		{
			// Must do action!
		}
	}
}

public class StartScreenView : BaseView
{	
}

public class AView : BaseView
{	
}

public class BView : BaseView
{	
}

public class UnrulyView : BaseView
{	
}