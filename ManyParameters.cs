// More than THREE parameters smells like one of two problems:
// 1. The function is doing too much

public int GetBirdAmountBetweenInTwoDurations(Date startMorning, Date endMorning, Date startEvening, Date endEvening)
{
	var birdsInMorning = DBController.GetBirdAmount(startMorning, endMorning);
	var birdsInEvening = DBController.GetBirdAmount(startEvening, endEvening);
	return birdsInMorning + birdsInEvening;
}

//2. There is another object hiding in there

public int SetSizeOfView(int x, int y, int width, int height)
{
	
}