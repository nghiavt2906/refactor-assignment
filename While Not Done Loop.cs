boolean done = false;
int idx = 0;
while (! done) {
	int idxNext = -1;
	if (! isSomeTest()) {
		done = true;
	} else {
		idxNext = findNext(idx);
	}
	if (idxNext >= 0) {
		doSomething(idx, idxNext);
	} else {
		done = true;
	}
	if (! done) {
		idx = idxNext;
	}
}