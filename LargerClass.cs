//Avoid large class

public class Employee
{
	public string Name 			{get;set;}	
	public string Age 			{get;set;}	
	public string PhoneCode 	{get;set;}	
	public string PhoneArea 	{get;set;}	
	public string PhoneNumber	{get;set;}	
	public string Address 		{get;set;}		
}