//Must not use constant number directly in code

double potentialEnergy(double mass, double height) {
  return mass * height * 9.81;
}